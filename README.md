# Spring Boot JWT

####

Run this project by this command :

`mvn clean spring-boot:run`

Go to your POSTMAN `http://localhost:8080/user/register` with the following request body:
```
{
 “name”: “Uzumaki Naruto”,
 “email”: “uzumaki_naruto@konohagakure.com”,
 “password”: “password”
}
```

We should get a token in the response body:
```
"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjcmVhdGVkQXQiOjE1MTgwMTQ4MzQsInVzZXJJZCI6IjVhN2IxMTcyMzVhODJkYjVlODQxMzE1NiJ9.jE6BgGdpFta4bVAHOAT8iQTUjhrKp7JijwBXwBTxS6Q"
```

Now copy this token and let’s try to get just created user by making GET request to `http://localhost:8080/user/get`, but do not forget to add our token as Authorization header:


If you don’t pass token or remove some character from it then you should get 401 Unauthorized error:

Conclusion. Now you know how to use JWT tokens and how to secure your API. But you can play around with it and make it more complicated, for example save all tokens to database and implement logging from different devices functionality.