package com.hendisantika.jwtexample.repository;

import com.hendisantika.jwtexample.model.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringBootJWT
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 06/02/18
 * Time: 20.52
 * To change this template use File | Settings | File Templates.
 */

@Repository
public interface UserRepository extends MongoRepository<User, ObjectId> {
}
